$("#carousel-button").click(function(){
    if ($("#carousel-button").children("span").hasClass("fa-pause")) {
        $("#mycarousel").carousel("pause");
        $("#carousel-button").children("span").removeClass("fa-pause");
        $("#carousel-button").children("span").addClass("fa-play");

        $("#carousel-button").removeClass("btn-danger");
        $("#carousel-button").addClass("btn-success");

        $("#NoG").text("How many hoes will you bring with ya?");
        $("#NoG").removeClass("col-lg-2");

    }
    else if ($("#carousel-button").children("span").hasClass("fa-play")){
        $("#mycarousel").carousel("cycle");
        $("#carousel-button").children("span").removeClass("fa-play");
        $("#carousel-button").children("span").addClass("fa-pause");

        $("#carousel-button").removeClass("btn-success");
        $("#carousel-button").addClass("btn-danger");

        $("#NoG").text("Number of Guests");
        $("#NoG").addClass("col-lg-2");

    }
});

$("#loginbutton").on("click", function(){
  $("#loginModal").modal("toggle");
});

$("#reserveButton").on("click", function(){
  $("#myModal").modal("toggle");
});

// $(“#loginbutton”).on(“click”, function(){
//      $(“#loginModal”).modal();
// });
// $(document).ready(function(){
//
// $('#loginModal').modal('toggle');
// $('#myModal').modal('toggle');
// )};

// Unused script for tooltip ;)
// <script>
// $(document).ready(function(){
//   $('[data-toggle="tooltip"]').tooltip();
// });
// </script>
